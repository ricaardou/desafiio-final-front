var express = require('express');
var app = express();
var path = require('path');

app.use(express.static(__dirname + '/public'));


app.get('/', function(req, res) {
    res.sendFile(path.join(__dirname + '/html/login.html'));
});

app.get('/inicio', function(req, res) {res.sendFile(path.join(__dirname + '/html/inicio.html'));});
app.get('/usuarios', function(req, res) {res.sendFile(path.join(__dirname + '/html/usuarios.html'));});
app.get('/productos', function(req, res) {res.sendFile(path.join(__dirname + '/html/productos.html'));});

app.listen(80, function () {
  console.log('Server Corriendo');
});