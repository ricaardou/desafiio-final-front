$(function() {
    load();   
});
let Usuarios = [];
load = ()=>{
    $.getJSON("http://localhost/desafio-final/public/api/productos", Response=>{
        
        var Renderer = function (instance, td, row, col, prop, value, cellProperties) {
            td.innerHTML = '<button class="btn btn-info" role="button" style="margin: 2px 5px;" onclick="update_producto(\''+Response[cellProperties.visualRow]._id+'\')">Editar</button><button onclick="delete_producto(\''+Response[cellProperties.visualRow]._id+'\')" class="btn btn-danger">Borrar</button>';
        };
        
        Usuarios = Response;

        var hotElement = document.querySelector('#hot');
        $(hotElement).empty();
        var hotElementContainer = hotElement.parentNode;
        var hotSettings = {
          data: Response,
          columns: [
            {
              data: 'CODIGO',
              type: 'text',
              width: 20
            },
             {
              data: 'NOMBRE',
              type: 'text',
            },
            {
              data: 'DETALLE',
              type: 'text'
            },
            {
              data: 'STOCK',
              width: 20,
            },
            {
              data: 'ACCIONES',
              renderer: Renderer,
              width: 54,
              readOnly: true
            },
            
          ],
          stretchH: 'all',
          //width: 805,
          autoWrapRow: true,
          //height: 487,
          maxRows: 22,
          rowHeaders: true,
          colHeaders: [
            'Codigo',
            'Nombre',
            'Detalle',
            'Cantidad',
            'Acciones'
          ]
        };
        var hot = new Handsontable(hotElement, hotSettings);
    });
}
crear_usuario = ()=>{
    $("#myModal").modal();
}
update_producto = id=>{
    id_actual = id;
    let Filtro = Usuarios.filter(A=>A._id===id)[0];
    $("#form-editar [name=CODIGO]").val(Filtro.CODIGO);
    $("#form-editar [name=DETALLE]").val(Filtro.DETALLE);
    $("#form-editar [name=NOMBRE]").val(Filtro.NOMBRE);
    $("#form-editar [name=STOCK]").val(Filtro.STOCK);

    $("#modal-editar").modal();
}
delete_producto = id=>{
    $.getJSON("http://localhost/desafio-final/public/api/producto/remove/"+id, Response=>{
        load();
    });
}
var id_actual = null;
$("#form-crear").on("submit", E=>{
	E.preventDefault();
	let Data = $(E.currentTarget).serializeFormJSON({});
	$.post("http://localhost/desafio-final/public/api/producto/create", { DATA: Data }, Response=>{
        load();
        $("#limpiar-campos").click();
        $("#myModal").modal('hide');
	});
});


$("#form-editar").on("submit", E=>{
    E.preventDefault();
    let Data = $(E.currentTarget).serializeFormJSON({});
    $.post("http://localhost/desafio-final/public/api/producto/"+id_actual, { DATA: Data }, Response=>{
        load();
        $("#modal-editar").modal('hide');
    });
});






(function ($) {
    $.fn.serializeFormJSON = function () {

        var o = {};
        var a = this.serializeArray();
        $.each(a, function () {
            if (o[this.name]) {
                if (!o[this.name].push) {
                    o[this.name] = [o[this.name]];
                }
                o[this.name].push(this.value || '');
            } else {
                o[this.name] = this.value || '';
            }
        });
        return o;
    };
})(jQuery);