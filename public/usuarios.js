$(function() {
    load();   
});
let Usuarios = [];
load = ()=>{
    $.getJSON("http://localhost/desafio-final/public/api/users", Response=>{
        
        var Renderer = function (instance, td, row, col, prop, value, cellProperties) {
            td.innerHTML = '<button class="btn btn-info" role="button" style="margin: 2px 5px;" onclick="update_user(\''+Response[cellProperties.visualRow]._id+'\')">Editar</button><button onclick="delete_user(\''+Response[cellProperties.visualRow]._id+'\')" class="btn btn-danger">Borrar</button>';
        };
        
        Usuarios = Response;

        var hotElement = document.querySelector('#hot');
        $(hotElement).empty();
        var hotElementContainer = hotElement.parentNode;
        var hotSettings = {
          data: Response,
          columns: [
            {
              data: 'RUN',
              type: 'text',
              width: 10
            },
            {
              data: 'NAME',
              type: 'text'
            },
            {
              data: 'ACCIONES',
              renderer: Renderer,
              width: 14,
              readOnly: true
            },
            
          ],
          stretchH: 'all',
          //width: 805,
          autoWrapRow: true,
          //height: 487,
          maxRows: 22,
          rowHeaders: true,
          colHeaders: [
            'Run',
            'Nombre',
            'Acciones'
          ]
        };
        var hot = new Handsontable(hotElement, hotSettings);
    });
}
crear_usuario = ()=>{
    $("#myModal").modal();
}
update_user = id=>{
    id_actual = id;
    let Filtro = Usuarios.filter(A=>A._id===id)[0];
    $("#form-editar [name=RUN]").val(Filtro.RUN);
    $("#form-editar [name=PASS]").val(Filtro.PASS);
    $("#form-editar [name=NAME]").val(Filtro.NAME);
    $("#modal-editar").modal();
}
delete_user = id=>{
    $.getJSON("http://localhost/desafio-final/public/api/user/remove/"+id, Response=>{
        load();
    });
}
var id_actual = null;
$("#form-crear").on("submit", E=>{
	E.preventDefault();
	let Data = $(E.currentTarget).serializeFormJSON({});
	$.post("http://localhost/desafio-final/public/api/user/create", { DATA: Data }, Response=>{
        load();
        $("#limpiar-campos").click();
        $("#myModal").modal('hide');
	});
});


$("#form-editar").on("submit", E=>{
    E.preventDefault();
    let Data = $(E.currentTarget).serializeFormJSON({});
    $.post("http://localhost/desafio-final/public/api/user/"+id_actual, { DATA: Data }, Response=>{
        load();
        $("#modal-editar").modal('hide');
    });
});






(function ($) {
    $.fn.serializeFormJSON = function () {

        var o = {};
        var a = this.serializeArray();
        $.each(a, function () {
            if (o[this.name]) {
                if (!o[this.name].push) {
                    o[this.name] = [o[this.name]];
                }
                o[this.name].push(this.value || '');
            } else {
                o[this.name] = this.value || '';
            }
        });
        return o;
    };
})(jQuery);