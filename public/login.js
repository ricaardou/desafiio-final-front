$(function() {
    
});
$("#login-form").on("submit", E=>{
	E.preventDefault();
	let Data = $(E.currentTarget).serializeFormJSON({});
	$.post("http://localhost/desafio-final/public/api/user/auth", { DATA: Data }, Response=>{
        Response = JSON.parse(Response);
		if(Response.length>0){
			location.href = "/inicio";
		}
        else{
            alert("Usuario no registrado.");
        }
	});
});






(function ($) {
    $.fn.serializeFormJSON = function () {

        var o = {};
        var a = this.serializeArray();
        $.each(a, function () {
            if (o[this.name]) {
                if (!o[this.name].push) {
                    o[this.name] = [o[this.name]];
                }
                o[this.name].push(this.value || '');
            } else {
                o[this.name] = this.value || '';
            }
        });
        return o;
    };
})(jQuery);